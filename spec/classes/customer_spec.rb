# frozen_string_literal: true

require 'spec_helper'

describe 'engagement_docs::customer' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:params) do
        { 'customer_name' => 'davita',
          'output_dir' => '/tmp/puppet_delivery_docs/' }
      end

      it { is_expected.to compile }

      it 'has customer.yaml file' do
        is_expected.to contain_file("/tmp/puppet_delivery_docs/#{params['customer_name']}/customer.yaml").with_content('')
      end
    end
  end
end
