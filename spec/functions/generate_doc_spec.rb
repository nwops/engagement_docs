require 'spec_helper'

describe 'engagement_docs::generate_doc' do
  it { is_expected.to run.with_params(2).and_return(4) }
  it { is_expected.to run.with_params(4).and_return(16) }
  it { is_expected.to run.with_params(nil).and_return(0) }
end
