# @summary Collects information about the engagement and builds docs.
#
# Builds the end of engagement docs
#
# @example
#   include engagement_docs
class engagement_docs(
  String $pse_name,
  String $pse_email,
  String $engagement_number,
  String $engagement_title,
  String $pe_master_hostname,
  String $pe_version,
  Stdlib::AbsolutePath $customer_logo,
  Hash $r10k,
  Hash $jvm_heaps,
  Enum['jumpstart', 'custom'] $service_type,
  String $control_repo_remote_url,
  Array[String] $templates_build_order,
  String $customer_name = $::customer_name,
  Enum['F5','HA proxy','apache', 'N/A'] $load_balancer_type,
  Enum['Gitlab','Github Enterprise','Stash', 'Bitbucket','VSTS','Azure DevOps'] $git_server_type,
  Enum['el-5-x86_64','el-6-x86_64','el-7-x86_64'] $pe_platform = 'el-7-x86_64',
  Enum['pdf', 'html', 'markdown', 'epub'] $rendered_doc_type = 'pdf',
  Stdlib::AbsolutePath $base_output_dir = '/tmp/pse_stuff/engagements',
  Enum['extra-large', 'extra-large', 'standard', 'standard-ha', 'large', 'large-ha'] $pe_architecture = 'standard',
  String $architecutre_diagram = "${pe_architecture}_diagram.png",
  Integer $jruby_max_requests_per_instance = 10000,
  Integer $classifier_synchronization_period = 600,
  String $docs_source_path = 'puppet:///modules/engagement_docs',
) {
  include engagement_docs::install

  # unique directory for the customer engagement
  $customer_dir = extlib::path_join([$base_output_dir, $customer_name, $engagement_number])
  # create all the directories if they don't already exist
  file{extlib::dir_split($customer_dir): ensure => directory }
  # The path to the customer.yaml file for legacy data usage
  $customer_file = extlib::path_join([$customer_dir, 'customer.yaml'])
  # A file where the build script is stored
  $build_script = extlib::path_join([$customer_dir, 'build_script.sh'])
  $module_dir = module_directory($module_name)
  # The build order of the docs, based on the legacy tooling
  $template_order_file = extlib::path_join([$customer_dir, 'template_order_file.yaml'])
  $customer_files_dir = "${customer_dir}/files"
  $customer_assets_dir = "${customer_files_dir}/docs/assets"
  $customer = {
    pse_name                => $pse_name,
    pse                     => $pse_name,
    logo                    => $customer_logo,
    pse_email               => $pse_email,
    engagement_title        => $engagement_title,
    engagement_number       => $engagement_number,
    service_type            => $service_type,
    pe_master_hostname      => $pe_master_hostname,
    pe_version              => $pe_version,
    pe_platform             => $pe_platform,
    load_balancer_type      => $load_balancer_type,
    git_server_type         => $git_server_type,
    control_repo_remote_url => $control_repo_remote_url,
    r10k                    => $r10k,
    jvm_heaps               => $jvm_heaps,
    jruby_max_requests_per_instance => $jruby_max_requests_per_instance,
  }
  # builds out the legacy customer file
  file{$customer_file:
    ensure  => present,
    content => epp('engagement_docs/customer.yaml.epp', {customer_details => $customer})
  }
  # assets directory is used to store images, docs and other items used for rendering
  file{$customer_assets_dir:
    source  => "${docs_source_path}/docs/assets",
    recurse => true
  }

  # The build files are all the files the PSE wants to use
  # in the final document output
  $build_files = $templates_build_order.unique.map | $file | {
    $file_path = "${customer_files_dir}/${file}"
    exec{"mkdir_p ${file_path}":
      command  => "mkdir -p $(dirname ${file_path})",
      provider => shell,
      unless   => "test -d $(dirname ${file_path})",
      before   => File[$file_path]
    }
    file { $file_path:
      ensure => 'present',
      #content => template("${module_dir}/files/${file}"),
      source => "${docs_source_path}/${file}",
      notify => Exec["generate_${rendered_doc_type}"]
    }
    $file_path
  }
  # This is the build order of all the build files 
  # For legacy purposes and reference
  file{$template_order_file:
    ensure  => present,
    content => $templates_build_order.unique.map |$file| { "${customer_files_dir}/${file}" }.to_yaml
  }

  # generate the document
  file{$build_script:
    ensure  => file,
    content => epp('engagement_docs/build_script.sh.epp',
      {
        output_file  => "${customer_dir}/engagement_doc.${rendered_doc_type}",
        build_files  => $build_files,
        customer_dir => "${customer_dir}/"
      }),
    mode    => '0774'
  }
  # <%= $build_files.regsubst($customer_dir, '').join(" \\ \n") %>
  # This is required when the document doesn't already exist 
  # and no content has changed
  # example, if the document file was deleted
  exec{"touch_${rendered_doc_type}":
      provider => shell,
      command  => "touch ${customer_dir}/engagement_doc.${rendered_doc_type}",
      creates  => "${customer_dir}/engagement_doc.${rendered_doc_type}",
      notify   => Exec["generate_${rendered_doc_type}"]
  }
  exec{"generate_${rendered_doc_type}":
      provider    => shell,
      command     => $build_script,
      refreshonly => true,
      subscribe   => File[$template_order_file, $customer_file, $build_script, $customer_assets_dir]
  }
}
