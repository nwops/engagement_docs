# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include engagement_docs::install
class engagement_docs::install(
  Optional[String] $package_provider,
  Array[String] $packages,
) {
  package{$packages:
    ensure   => present,
    provider => $package_provider
  }
}
