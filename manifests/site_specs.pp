# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include engagement_docs::pe_specs
class engagement_docs::site_specs(
  Hash $jvm_heaps,
  Hash $r10k_details,
  Enum['extra-large', 'extra-large', 'standard', 'standard-ha', 'large', 'large-ha'] $pe_architecture = 'standard',
  String $architecutre_diagram = "${pe_architecture}_diagram.png",
  Integer $jruby_max_requests_per_instance = 10000,
  Integer $classifier_synchronization_period = 600,

) {
}
