# Engagement Docs

This module is used for Puppet Professional Services to help generate the leave behind docs.

Why puppet code?  Because we can, and we should.  Puppet is a tool that we are all familar with.  We should be using it for
various use cases.  

This is a WIP/POC.  It does not fully work but the framework is here to complete later.

## Table of Contents

- [Engagement Docs](#engagement-docs)
  - [Table of Contents](#table-of-contents)
  - [Description](#description)
  - [Setup](#setup)
    - [Setup Requirements](#setup-requirements)
    - [Beginning with engagement_docs](#beginning-with-engagement_docs)
  - [Usage](#usage)
    - [Hiera Usage](#hiera-usage)
    - [Example Customer usage](#example-customer-usage)
  - [Limitations](#limitations)
  - [Development](#development)

## Description
A puppet module tool to generate leave behind docs for Puppet Service Professionals. 
One of the biggest pains with the leave behind docs is remembering how to use the tool to generate them.
Since this is in puppet code we cannot forget how to use.  Additionally, we can customize defaults for just about anything
we want.  

As for generating the pdf and other document types, the [pandoc](https://pandoc.org) document generator is the defacto standard for converting from one format to another.  Installable on all platforms.

## Setup

### Setup Requirements 
Currently this only works on a mac which is primaryly dependent on the pandoc tool [installation procedures](https://pandoc.org/installing.html). There is support for windows, linux and CI tooling as well but it has not been added to this code yet.  Please make a PR.

### Beginning with engagement_docs
1. You will need brew install if using OS X.
2. Have a hiera.yaml and data directory setup to store your customer data in.
3. Have downloaded this module and placed in your module's directory.  `puppet config print basemodulepath`
4. You have run `git submodule update --init --recursive --jobs 4`

This module uses git submodules to keep the docs up to date. See this [doc](https://www.vogella.com/tutorials/GitSubmodules/article.html) if you are not familar with submodules.


## Usage
1. Create a manifest
2. Run puppet apply with the manifest

If you only cloned the module and are working within the module itself.  Use the following:

1. pdk bundle exec rake spec_prep
2. pdk bundle exec puppet apply --basemodulepath=spec/fixtures/modules examples/customer1_jumpstart.pp
### Hiera Usage
Hiera is used to fill in many details and parameters.  There can be two levels.  The module data, and the global hiera data located in your home directory. 

You can use the default hiera_config location `puppet config print hiera_config` or create a custom hiera.yaml file and supply that file using the `--hiera_config` flag to pass in the hiera.yaml location using puppet.

### Example Customer usage
The following will create the customer.yaml file and auto generate the leave behind docs using pandoc document generator.  


```puppet
# examples/customer1_jumpstart.pp
$customer_name = 'customer1'
$service_type = 'jumpstart'
$engagement_number = '123456'
class{'engagement_docs':
  service_type      => $service_type,
  base_output_dir   => lookup('engagement_docs::base_output_dir', {default_value => '/Users/user1/sdp_stuff/engagements'}),
  engagement_number => $engagement_number,
  engagement_title  => 'Paired Programming'
}
# Needless default lookup intended for illustration purposes
```

`puppet apply --hiera_config=/Users/user1/sdp_stuff/engagements/hiera.yaml customer1_jumpstart.pp`

This will output the following files
1. /Users/user1/sdp_stuff/engagements/customer1/123456/customer.yaml
2. /Users/user1/sdp_stuff/engagements/customer1/123456/template_order_file.yaml
3. /Users/user1/sdp_stuff/engagements/customer1/123456/engagement_doc.pdf


```shell
(base) ➜  engagement_docs git:(master) ✗ puppet apply --modulepath=spec/fixtures/modules --hiera_config=hiera.yaml examples/customer1_jumpstart.pp
Notice: Compiled catalog for macbook-pro-162.lan in environment production in 0.21 seconds
Notice: /Stage[main]/Engagement_docs/File[/Users/cosman/sdp_stuff/engagements/customer1]/ensure: created
Notice: /Stage[main]/Engagement_docs/File[/Users/cosman/sdp_stuff/engagements/customer1/123456]/ensure: created
Notice: /Stage[main]/Engagement_docs/File[/Users/cosman/sdp_stuff/engagements/customer1/123456/customer.yaml]/ensure: defined content as '{md5}9ec88b1f194ed8b68f2c67a801340ec6'
Notice: /Stage[main]/Engagement_docs/File[/Users/cosman/sdp_stuff/engagements/customer1/123456/template_order_file.yaml]/ensure: defined content as '{md5}6d2abbcdec3b5f1a1e847652a9589be3'
Notice: /Stage[main]/Engagement_docs::Install/Package[homebrew/cask/basictex]/ensure: created
Notice: /Stage[main]/Engagement_docs/Exec[generate_pdf]/returns: executed successfully
Notice: Applied catalog in 7.63 seconds
```

## Limitations

This is a POC so doesn't really work right now.

## Development