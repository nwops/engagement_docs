# these variables are required for hiera lookups to occur
$customer_name = 'customer1'   # used in hiera lookup, required
$service_type = 'jumpstart'    # used in hiera lookup, required
$engagement_number = '123456'  # used in hiera lookup, required
class{'engagement_docs':
  service_type      => $service_type,
  base_output_dir   => '/Users/cosman/sdp_stuff/engagements',
  engagement_number => $engagement_number,
  engagement_title  => 'Paired Programming',
  git_server_type   => 'Gitlab',
}


