# See https://docs.puppet.com/puppet/4.5/reference/lang_write_functions_in_puppet.html
# for more information on native puppet functions.
function engagement_docs::generate_doc(Stdlib::AbsolutePath $output_dir, String $input_file , Enum[pdf] $document_type = 'pdf') {
    $config = {
      'customer_data'=>'customer.yaml',
      'templates_list'=>'templates.yaml',
      'dictionary_file'=>'dictionary/aspell.en.pws',
      'cover_template'=>'assets/cover.erb',
      'stylesheet'=>'assets/style.css',
      'pdf_filename'=>'lint_report.pdf',
      'page_size'=>'Letter',
      'toc'=>true,
      'footer_font_name'=>'Open Sans',
      'footer_font_size'=>'10',
      'footer_left'=>'[date]',
      'footer_right'=>'Page [page] of [topage]',
      'footer_spacing'=>'2.5',
      'margin_top'=>'1.00in',
      'margin_bottom'=>'1.00in',
      'margin_left'=>'0.50in',
      'margin_right'=>'0.50in',
      'xsl_stylesheet'=>'assets/toc.xsl',
      'after_title_page'=>'always',
      'after_each_section'=>'always',
      'back_cover'=>'always',
      'glossary'=>'false'
    }

    # exec{"generate_${document_type}":
    #   provider => shell,
    #   command => "pandoc -f markdown -t pdf -o ${output_dir}/engagement_doc ${input_file}"
    # }
}
