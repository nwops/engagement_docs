
# See https://docs.puppet.com/puppet/4.5/reference/lang_write_functions_in_puppet.html
# for more information on native puppet functions.
function engagement_docs::pe_versions() >> Array {
  $current_year = Integer(strftime('%Y'))
  range(2015,$current_year).map |$x| {
    range(1,4).map |$z| {
      range(1,10).map |$y| {
        "${x}.${y}.${z}"
      }
    }
  }.flatten.sort
}
