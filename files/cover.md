<div class="cover wrapper">
  <img class="cover logo" src="<%= File.expand_path("./assets/images/puppet-logo-amber-black.svg") %>">

  <h1><%= @customer['engagement_title'] || 'Consulting Engagement Leave-Behind Documents' %></h1>

  <div class="cover footer">
    Prepared on <%= Time.now.strftime("%-d %B %Y") %> for <%= @customer['name'] %> by <%= @customer['pse'] %> on behalf of Puppet, Inc.
  </div>

  <% if File.exists?(@customer['logo']) %>
    <img class="cover customer logo" src="<%= File.expand_path(@customer['logo']) %>">
  <% end %>
</div>